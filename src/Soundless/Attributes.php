<?php
declare(strict_types = 1);
namespace Soundless\Attributes;

use Closure;
use TypeError;
use function Functional\filter;
use function Functional\flatten;


// Basic attribute constructors

/**
 * @param string $name
 * @param string $value
 * @return StringAttribute
 */
function stringAttribute(string $name, string $value): StringAttribute
{
    return new StringAttribute($name, $value);
}

/**
 * @param string $name
 * @param bool $value
 * @return BoolAttribute
 */
function boolAttribute(string $name, bool $value = true): BoolAttribute
{
    return new BoolAttribute($name, $value);
}

/**
 * @param string $name
 * @param int $value
 * @return IntAttribute
 */
function intAttribute(string $name, int $value): IntAttribute
{
    return new IntAttribute($name, $value);
}

/**
 * @param string $name
 * @param float $value
 * @return FloatAttribute
 */
function floatAttribute(string $name, float $value): FloatAttribute
{
    return new FloatAttribute($name, $value);
}

/**
 * @param string $name
 * @param string $separator
 * @return Closure (string ...$list): StringListAttribute
 */
function stringListAttribute(string $name, string $separator): Closure
{
    return function (string ...$list) use (
        $name,
        $separator
    ): StringListAttribute {
        return new StringListAttribute($name, $separator, $list);
    };
}

/**
 * @param string $name
 * @param string $separator
 * @return Closure (int ...$list): IntListAttribute
 */
function intListAttribute(string $name, string $separator): Closure
{
    return function (int ...$list) use (
        $name,
        $separator
    ): IntListAttribute {
        return new IntListAttribute($name, $separator, $list);
    };
}

/**
 * @param array $styles
 * @return Style
 */
function style(array $styles): Style
{
    return new Style($styles);
}

/**
 * @param string $attributeString
 * @return RawAttributes
 */
function rawAttributes(string $attributeString): RawAttributes
{
    return new RawAttributes($attributeString);
}

/**
 * @return None
 */
function none(): None
{
    return new None();
}


// Helpers

/**
 * @param bool[] $classList [string => bool]
 * @return StringListAttribute
 */
function classList($classList): StringListAttribute
{
    $activeClasses = array_keys(
        filter(
            $classList,
            function (bool $value) {
                return $value;
            }));
    return stringListAttribute('class', ' ')(...$activeClasses);
}

/**
 * @param int $x
 * @param int $y
 * @param int $r
 * @return Attribute[]
 */
function circleShape(int $x, int $y, int $r)
{
    return [shape('circle'), coords($x, $y, $r)];
}

/**
 * @param int $left
 * @param int $top
 * @param int $right
 * @param int $bottom
 * @return Attribute[]
 */
function rectShape(int $left, int $top, int $right, int $bottom)
{
    return [shape('rect'), coords($left, $top, $right, $bottom)];
}

/**
 * @param int[] ...$points [int $x, int $y]
 * @return Attribute[]
 */
function polyShape(...$points)
{
    $flatPoints = flatten($points);
    return [shape('poly'), coords(...$flatPoints)];
}

/**
 * @return Attribute[]
 */
function defaultShape()
{
    return [shape('default')];
}


// HTML attributes

/**
 * @param string[] $list
 * @return StringListAttribute
 */
function accept(string ...$list): StringListAttribute
{
    return stringListAttribute('accept', ',')(...$list);
}

/**
 * @param string[] $list
 * @return StringListAttribute
 */
function acceptCharset(string ...$list): StringListAttribute
{
    return stringListAttribute('accept-charset', ' ')(...$list);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function accesskey(string $value): StringAttribute
{
    return stringAttribute('accesskey', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function action(string $value): StringAttribute
{
    return stringAttribute('action', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function align(string $value): StringAttribute
{
    return stringAttribute('align', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function alt(string $value): StringAttribute
{
    return stringAttribute('alt', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function async(bool $value = true): BoolAttribute
{
    return boolAttribute('async', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function autocomplete(bool $value = true): BoolAttribute
{
    return boolAttribute('autocomplete', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function autofocus(bool $value = true): BoolAttribute
{
    return boolAttribute('autofocus', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function autoplay(bool $value = true): BoolAttribute
{
    return boolAttribute('autoplay', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function autosave(bool $value = true): BoolAttribute
{
    return boolAttribute('autosave', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function charset(string $value): StringAttribute
{
    return stringAttribute('charset', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function checked(bool $value = true): BoolAttribute
{
    return boolAttribute('checked', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function cite(string $value): StringAttribute
{
    return stringAttribute('cite', $value);
}

/**
 * @param string[] ...$list
 * @return StringListAttribute
 */
function class_(string ...$list): StringListAttribute
{
    return stringListAttribute('class', ' ')(...$list);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function cols(int $value): IntAttribute
{
    return intAttribute('cols', $value);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function colspan(int $value): IntAttribute
{
    return intAttribute('colspan', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function content(string $value): StringAttribute
{
    return stringAttribute('content', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function contenteditable(bool $value = true): BoolAttribute
{
    return boolAttribute('contenteditable', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function contextmenu(string $value): StringAttribute
{
    return stringAttribute('contextmenu', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function controls(bool $value = true): BoolAttribute
{
    return boolAttribute('controls', $value);
}

/**
 * @param int[] ...$list
 * @return IntListAttribute
 */
function coords(int ...$list): IntListAttribute
{
    return intListAttribute('coords', ',')(...$list);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function crossorigin(string $value): StringAttribute
{
    return stringAttribute('crossorigin', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function data(string $value): StringAttribute
{
    return stringAttribute('data', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function datetime(string $value): StringAttribute
{
    return stringAttribute('datetime', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function default_(bool $value = true): BoolAttribute
{
    return boolAttribute('default', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function defer(bool $value = true): BoolAttribute
{
    return boolAttribute('defer', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function dir(string $value): StringAttribute
{
    return stringAttribute('dir', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function dirname(string $value): StringAttribute
{
    return stringAttribute('dirname', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function disabled(bool $value = true): BoolAttribute
{
    return boolAttribute('disabled', $value);
}

/**
 * @param bool|string $value
 * @return Attribute
 */
function download($value = true): Attribute
{
    if (is_string($value)) {
        return stringAttribute('download', $value);
    }

    if (is_bool($value)) {
        return boolAttribute('download', $value);
    }

    throw new TypeError();
}

/**
 * @param string $value
 * @return StringAttribute
 */
function draggable(string $value): StringAttribute
{
    return stringAttribute('draggable', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function dropzone(string $value): StringAttribute
{
    return stringAttribute('dropzone', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function enctype(string $value): StringAttribute
{
    return stringAttribute('enctype', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function for_(string $value): StringAttribute
{
    return stringAttribute('for', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function form(string $value): StringAttribute
{
    return stringAttribute('form', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function formaction(string $value): StringAttribute
{
    return stringAttribute('formaction', $value);
}

/**
 * @param string[] $list
 * @return StringListAttribute
 */
function headers(string ...$list): StringListAttribute
{
    return stringListAttribute('headers', ' ')(...$list);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function height(int $value): IntAttribute
{
    return intAttribute('height', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function hidden(bool $value = true): BoolAttribute
{
    return boolAttribute('hidden', $value);
}

/**
 * @param float $value
 * @return FloatAttribute
 */
function high(float $value): FloatAttribute
{
    return new FloatAttribute('high', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function href(string $value): StringAttribute
{
    return stringAttribute('href', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function hreflang(string $value): StringAttribute
{
    return stringAttribute('hreflang', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function httpEquiv(string $value): StringAttribute
{
    return stringAttribute('httpEquiv', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function id(string $value): StringAttribute
{
    return stringAttribute('id', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function integrity(string $value): StringAttribute
{
    return stringAttribute('integrity', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function ismap(bool $value = true): BoolAttribute
{
    return boolAttribute('ismap', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function itemprop(string $value): StringAttribute
{
    return stringAttribute('itemprop', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function kind(string $value): StringAttribute
{
    return stringAttribute('kind', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function label(string $value): StringAttribute
{
    return stringAttribute('label', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function lang(string $value): StringAttribute
{
    return stringAttribute('lang', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function language(string $value): StringAttribute
{
    return stringAttribute('language', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function list_(string $value): StringAttribute
{
    return stringAttribute('list', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function loop(bool $value = true): BoolAttribute
{
    return boolAttribute('loop', $value);
}

/**
 * @param float $value
 * @return FloatAttribute
 */
function low(float $value): FloatAttribute
{
    return floatAttribute('low', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function manifest(string $value): StringAttribute
{
    return stringAttribute('manifest', $value);
}

/**
 * @param float $value
 * @return FloatAttribute
 */
function max(float $value): FloatAttribute
{
    return floatAttribute('max', $value);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function maxlength(int $value): IntAttribute
{
    return intAttribute('maxlength', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function media(string $value): StringAttribute
{
    return stringAttribute('media', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function method(string $value): StringAttribute
{
    return stringAttribute('method', $value);
}

/**
 * @param float $value
 * @return FloatAttribute
 */
function min(float $value): FloatAttribute
{
    return floatAttribute('min', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function multiple(bool $value = true): BoolAttribute
{
    return boolAttribute('multiple', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function muted(bool $value = true): BoolAttribute
{
    return boolAttribute('muted', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function name(string $value): StringAttribute
{
    return stringAttribute('name', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function novalidate(bool $value = true): BoolAttribute
{
    return boolAttribute('novalidate', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function open(bool $value = true): BoolAttribute
{
    return boolAttribute('open', $value);
}

/**
 * @param float $value
 * @return FloatAttribute
 */
function optimum(float $value): FloatAttribute
{
    return floatAttribute('optimum', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function pattern(string $value): StringAttribute
{
    return stringAttribute('pattern', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function ping(string $value): StringAttribute
{
    return stringAttribute('ping', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function placeholder(string $value): StringAttribute
{
    return stringAttribute('placeholder', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function poster(string $value): StringAttribute
{
    return stringAttribute('poster', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function preload(string $value): StringAttribute
{
    return stringAttribute('preload', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function radiogroup(string $value): StringAttribute
{
    return stringAttribute('radiogroup', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function readonly(bool $value = true): BoolAttribute
{
    return boolAttribute('readonly', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function rel(string $value): StringAttribute
{
    return stringAttribute('rel', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function required(bool $value = true): BoolAttribute
{
    return boolAttribute('required', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function reversed(bool $value = true): BoolAttribute
{
    return boolAttribute('reversed', $value);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function rows(int $value): IntAttribute
{
    return intAttribute('rows', $value);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function rowspan(int $value): IntAttribute
{
    return intAttribute('rowspan', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function sandbox(bool $value = true): BoolAttribute
{
    return boolAttribute('sandbox', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function scope(string $value): StringAttribute
{
    return stringAttribute('scope', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function scoped(bool $value = true): BoolAttribute
{
    return boolAttribute('scoped', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function seamless(bool $value = true): BoolAttribute
{
    return boolAttribute('seamless', $value);
}

/**
 * @param bool $value
 * @return BoolAttribute
 */
function selected(bool $value = true): BoolAttribute
{
    return boolAttribute('selected', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function shape(string $value): StringAttribute
{
    return stringAttribute('shape', $value);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function size(int $value): IntAttribute
{
    return intAttribute('size', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function sizes(string $value): StringAttribute
{
    return stringAttribute('sizes', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function slot(string $value): StringAttribute
{
    return stringAttribute('slot', $value);
}

/**
 * @param int $value
 * @return IntAttribute
 */
function span(int $value): IntAttribute
{
    return intAttribute('span', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function spellcheck(string $value): StringAttribute
{
    return stringAttribute('spellcheck', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function src(string $value): StringAttribute
{
    return stringAttribute('src', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function srcdoc(string $value): StringAttribute
{
    return stringAttribute('srcdoc', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function srclang(string $value): StringAttribute
{
    return stringAttribute('srclang', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function srcset(string $value): StringAttribute
{
    return stringAttribute('srcset', $value);
}

/**
 * @param float|string $value
 * @return Attribute
 * @throws TypeError
 */
function step($value): Attribute
{
    if (is_float($value)) {
        return floatAttribute('step', $value);
    }

    if (is_string($value)) {
        return stringAttribute('step', $value);
    }

    throw new TypeError();
}

/**
 * @param int $value
 * @return IntAttribute
 */
function tabindex(int $value): IntAttribute
{
    return intAttribute('tabindex', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function target(string $value): StringAttribute
{
    return stringAttribute('target', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function title(string $value): StringAttribute
{
    return stringAttribute('title', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function type(string $value): StringAttribute
{
    return stringAttribute('type', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function usemap(string $value): StringAttribute
{
    return stringAttribute('usemap', $value);
}

/**
 * @param string|int|float $value
 * @return Attribute
 */
function value($value): Attribute
{
    if (is_string($value)) {
        return stringAttribute('value', $value);
    }

    if (is_int($value)) {
        return intAttribute('value', $value);
    }

    if (is_float($value)) {
        return floatAttribute('value', $value);
    }

    throw new TypeError();
}

/**
 * @param int $value
 * @return IntAttribute
 */
function width(int $value): IntAttribute
{
    return intAttribute('width', $value);
}

/**
 * @param string $value
 * @return StringAttribute
 */
function wrap(string $value): StringAttribute
{
    return stringAttribute('wrap', $value);
}



// Data model

interface Attribute
{
}

class StringAttribute implements Attribute
{
    /** @var string */
    private $name;
    /** @var string */
    private $value;

    /**
     * @param string $name
     * @param string $value
     */
    public function __construct(string $name, string $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}

class BoolAttribute implements Attribute
{
    /** @var string */
    private $name;
    /** @var bool */
    private $value;

    /**
     * @param string $name
     * @param bool $value
     */
    public function __construct(string $name, bool $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getValue(): bool
    {
        return $this->value;
    }
}

class IntAttribute implements Attribute
{
    /** @var string */
    private $name;
    /** @var int */
    private $value;

    /**
     * @param string $name
     * @param int $value
     */
    public function __construct(string $name, int $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }
}

class FloatAttribute implements Attribute
{
    /** @var string */
    private $name;
    /** @var float */
    private $value;

    /**
     * @param string $name
     * @param float $value
     */
    public function __construct(string $name, float $value)
    {
        $this->name = $name;
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }
}

class StringListAttribute implements Attribute
{
    /** @var string */
    private $name;
    /** @var string */
    private $separator;
    /** @var string[] */
    private $list;

    /**
     * @param string $name
     * @param string $separator
     * @param string[] $list
     */
    public function __construct(string $name, string $separator, $list)
    {
        $this->name = $name;
        $this->separator = $separator;
        $this->list = $list;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * @return string[]
     */
    public function getList()
    {
        return $this->list;
    }
}

class IntListAttribute implements Attribute
{
    /** @var string */
    private $name;
    /** @var string */
    private $separator;
    /** @var int[] */
    private $list;

    /**
     * @param string $name
     * @param string $separator
     * @param int[] $list
     */
    public function __construct(string $name, string $separator, $list)
    {
        $this->name = $name;
        $this->separator = $separator;
        $this->list = $list;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getSeparator(): string
    {
        return $this->separator;
    }

    /**
     * @return int[]
     */
    public function getList()
    {
        return $this->list;
    }
}

class Style implements Attribute
{
    /** @var array */
    private $styles;

    /**
     * @param array $styles [string => string]
     */
    public function __construct(array $styles)
    {
        $this->styles = $styles;
    }

    /**
     * @return array
     */
    public function getStyles(): array
    {
        return $this->styles;
    }
}

class RawAttributes implements Attribute
{
    /** @var string */
    private $attributeString;

    /**
     * @param string $attributeString
     */
    public function __construct(string $attributeString)
    {
        $this->attributeString = $attributeString;
    }

    /**
     * @return string
     */
    public function getAttributeString(): string
    {
        return $this->attributeString;
    }
}

class None implements Attribute
{
}