<?php
declare(strict_types=1);

namespace Soundless;

use Closure;
use Soundless\Attributes\Attribute;
use Soundless\Attributes\BoolAttribute;
use Soundless\Attributes\FloatAttribute;
use Soundless\Attributes\IntAttribute;
use Soundless\Attributes\RawAttributes;
use Soundless\Attributes\StringAttribute;
use Soundless\Attributes\StringListAttribute;
use Soundless\Attributes\Style;
use TypeError;
use function Functional\intersperse;

/**
 * @param Node $node
 * @return string
 */
function renderToString(Node $node): string
{
    /**
     * @param string $string
     * @return string
     */
    $escape = function (string $string): string {
        return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
    };

    /**
     * @param Attribute[] $attributes
     * @return string
     */
    $renderAttributes = function ($attributes) use ($escape): string {
        return implode('', \Functional\map(
            $attributes,
            function (Attribute $attribute) use ($escape): string {
                if ($attribute instanceof StringAttribute) {
                    $name = $attribute->getName();
                    $value = $escape($attribute->getValue());
                    return " $name=\"$value\"";
                }

                if ($attribute instanceof BoolAttribute) {
                    $name = $attribute->getName();
                    $value = $attribute->getValue();
                    return $value ? " $name=\"\"" : '';
                }

                if ($attribute instanceof IntAttribute) {
                    $name = $attribute->getName();
                    $value = $escape((string)$attribute->getValue());
                    return " $name=\"$value\"";
                }

                if ($attribute instanceof FloatAttribute) {
                    $name = $attribute->getName();
                    $value = $escape((string)$attribute->getValue());
                    return " $name=\"$value\"";
                }

                if ($attribute instanceof StringListAttribute) {
                    $list = $attribute->getList();
                    if (count($list) > 0) {
                        $name = $attribute->getName();
                        $separator = $attribute->getSeparator();
                        $value = $escape(implode($separator, $list));
                        return " $name=\"$value\"";
                    }

                    return '';
                }

                if ($attribute instanceof Style) {
                    $styleStrings = \Functional\map(
                        $attribute->getStyles(),
                        function (string $value, string $key): string {
                            return "$key:$value";
                        });
                    $value = $escape(implode(';', $styleStrings));
                    return " style=\"$value\"";
                }

                if ($attribute instanceof RawAttributes) {
                    return " {$attribute->getAttributeString()}";
                }

                if ($attribute instanceof Attributes\None) {
                    return '';
                }

                throw new TypeError();
            }));
    };

    /**
     * @param Node[] $children
     * @return string
     */
    $renderChildren = function ($children) {
        return implode(
            '', \Functional\map($children, '\Soundless\renderToString'));
    };

    if ($node instanceof Element) {
        $name = $node->getName();
        $attributes = $renderAttributes($node->getAttributes());
        $innerHtml = $renderChildren($node->getChildren());
        return "<$name$attributes>$innerHtml</$name>";
    }

    if ($node instanceof EmptyElement) {
        $name = $node->getName();
        $attributes = $renderAttributes($node->getAttributes());
        return "<$name$attributes/>";
    }

    if ($node instanceof Text) {
        return $escape($node->getValue());
    }

    if ($node instanceof RawHtml) {
        return $node->getHtml();
    }

    if ($node instanceof RootElement) {
        $attributes = $renderAttributes($node->getAttributes());
        $innerHtml = $renderChildren($node->getChildren());
        return "<!DOCTYPE html><html$attributes>$innerHtml</html>";
    }

    if ($node instanceof None) {
        return '';
    }

    throw new TypeError();
}


// Basic constructor functions

/**
 * @param string $name
 * @return Closure
 */
function element(string $name): Closure
{
    return function (Attribute ...$attributes) use ($name): Closure {
        return function (Node ...$children) use ($name, $attributes): Element {
            return new Element($name, $attributes, $children);
        };
    };
}

/**
 * @param string $name
 * @return Closure
 */
function emptyElement(string $name): Closure
{
    return function (Attribute ...$attributes) use ($name) : EmptyElement {
        return new EmptyElement($name, $attributes);
    };
}

/**
 * @param string $value
 * @return Text
 */
function text(string $value): Text
{
    return new Text($value);
}

/**
 * @param string $html
 * @return RawHtml
 */
function rawHtml(string $html): RawHtml
{
    return new RawHtml($html);
}

/**
 * @return None
 */
function none(): None
{
    return new None();
}


// Helpers

/**
 * nl2br() equivalent for Soundless
 *
 * multilineText("foo\nbar") === [text('foo'), br (), text('bar')]
 *
 * p () ( ...multilineText($description) )
 *
 * @param string $value
 * @return Node[]
 */
function multilineText(string $value)
{
    $textNodes = \Functional\map(explode("\n", $value), '\Soundless\text');
    return intersperse($textNodes, br());
}


// Main root

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function html(Attribute ...$attributes): Closure
{
    return function (Node ...$children) use ($attributes): RootElement {
        return new RootElement($attributes, $children);
    };
}


// Document metadata

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function base(Attribute ...$attributes): Closure
{
    return emptyElement('base')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure (Node ...$children): Element
 */
function head(Attribute ...$attributes): Closure
{
    return element('head')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function link(Attribute ...$attributes): Closure
{
    return emptyElement('link')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function meta(Attribute ...$attributes): Closure
{
    return emptyElement('meta')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure (string $styles): Closure
 */
function style(Attribute ...$attributes): Closure
{
    return function (string $styles) use ($attributes): Closure {
        return element('style')(...$attributes)(rawHtml($styles));
    };
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function title(Attribute ...$attributes): Closure
{
    return element('title')(...$attributes);
}


// Content sectioning

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function address(Attribute ...$attributes): Closure
{
    return element('address')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function article(Attribute ...$attributes): Closure
{
    return element('article')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function aside(Attribute ...$attributes): Closure
{
    return element('aside')(...$attributes);
}

/**
 * @param Attribute[] ...$attributes
 * @return Closure
 */
function body(Attribute ...$attributes): Closure
{
    return element('body')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function footer(Attribute ...$attributes): Closure
{
    return element('footer')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function h1(Attribute ...$attributes): Closure
{
    return element('h1')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function h2(Attribute ...$attributes): Closure
{
    return element('h2')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function h3(Attribute ...$attributes): Closure
{
    return element('h3')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function h4(Attribute ...$attributes): Closure
{
    return element('h4')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function h5(Attribute ...$attributes): Closure
{
    return element('h5')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function h6(Attribute ...$attributes): Closure
{
    return element('h6')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function header(Attribute ...$attributes): Closure
{
    return element('header')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function hgroup(Attribute ...$attributes): Closure
{
    return element('hgroup')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function nav(Attribute ...$attributes): Closure
{
    return element('nav')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function section(Attribute ...$attributes): Closure
{
    return element('section')(...$attributes);
}


// Text content

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function dd(Attribute ...$attributes): Closure
{
    return element('dd')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function div(Attribute ...$attributes): Closure
{
    return element('div')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function dl(Attribute ...$attributes): Closure
{
    return element('dl')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function dt(Attribute ...$attributes): Closure
{
    return element('dt')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function figcaption(Attribute ...$attributes): Closure
{
    return element('figcaption')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function figure(Attribute ...$attributes): Closure
{
    return element('figure')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function hr(Attribute ...$attributes): Closure
{
    return emptyElement('hr')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function li(Attribute ...$attributes): Closure
{
    return element('li')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function main(Attribute ...$attributes): Closure
{
    return element('main')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function ol(Attribute ...$attributes): Closure
{
    return element('ol')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function p(Attribute ...$attributes): Closure
{
    return element('p')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function pre(Attribute ...$attributes): Closure
{
    return element('pre')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function ul(Attribute ...$attributes): Closure
{
    return element('ul')(...$attributes);
}


// Inline text semantics

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function a(Attribute ...$attributes): Closure
{
    return element('a')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function abbr(Attribute ...$attributes): Closure
{
    return element('a')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function b(Attribute ...$attributes): Closure
{
    return element('b')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function bdi(Attribute ...$attributes): Closure
{
    return element('bdi')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function bdo(Attribute ...$attributes): Closure
{
    return element('bdo')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function br(Attribute ...$attributes): Closure
{
    return emptyElement('br')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function cite(Attribute ...$attributes): Closure
{
    return element('cite')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function code(Attribute ...$attributes): Closure
{
    return element('code')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function data(Attribute ...$attributes): Closure
{
    return element('data')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function dfn(Attribute ...$attributes): Closure
{
    return element('dfn')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function em(Attribute ...$attributes): Closure
{
    return element('em')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function i(Attribute ...$attributes): Closure
{
    return element('i')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function kbd(Attribute ...$attributes): Closure
{
    return element('kbd')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function mark(Attribute ...$attributes): Closure
{
    return element('mark')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function q(Attribute ...$attributes): Closure
{
    return element('q')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function rp(Attribute ...$attributes): Closure
{
    return element('rp')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function rt(Attribute ...$attributes): Closure
{
    return element('rt')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function rtc(Attribute ...$attributes): Closure
{
    return element('rtc')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function ruby(Attribute ...$attributes): Closure
{
    return element('ruby')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function s(Attribute ...$attributes): Closure
{
    return element('s')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function samp(Attribute ...$attributes): Closure
{
    return element('samp')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function small(Attribute ...$attributes): Closure
{
    return element('small')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function span(Attribute ...$attributes): Closure
{
    return element('span')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function strong(Attribute ...$attributes): Closure
{
    return element('strong')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function sub(Attribute ...$attributes): Closure
{
    return element('sub')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function time(Attribute ...$attributes): Closure
{
    return element('time')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function u(Attribute ...$attributes): Closure
{
    return element('u')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function var_(Attribute ...$attributes): Closure
{
    return element('var')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function wbr(Attribute ...$attributes): Closure
{
    return emptyElement('wbr')(...$attributes);
}


// Image and multimedia

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function area(Attribute ...$attributes): Closure
{
    return emptyElement('area')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function audio(Attribute ...$attributes): Closure
{
    return element('audio')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function img(Attribute ...$attributes): Closure
{
    return emptyElement('img')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function map(Attribute ...$attributes): Closure
{
    return element('map')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function track(Attribute ...$attributes): Closure
{
    return emptyElement('track')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function video(Attribute ...$attributes): Closure
{
    return element('video')(...$attributes);
}


// Embedded content

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function embed(Attribute ...$attributes): Closure
{
    return emptyElement('embed')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function object(Attribute ...$attributes): Closure
{
    return element('object')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function param(Attribute ...$attributes): Closure
{
    return emptyElement('param')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function source(Attribute ...$attributes): Closure
{
    return emptyElement('source')(...$attributes);
}


// Scripting

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function canvas(Attribute ...$attributes): Closure
{
    return emptyElement('canvas')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function noscript(Attribute ...$attributes): Closure
{
    return element('noscript')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function script(Attribute ...$attributes): Closure
{
    return element('script')(...$attributes);
}


// Demarcating edits

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function del(Attribute ...$attributes): Closure
{
    return element('del')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function ins(Attribute ...$attributes): Closure
{
    return element('ins')(...$attributes);
}


// Table content

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function caption(Attribute ...$attributes): Closure
{
    return element('caption')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function col(Attribute ...$attributes): Closure
{
    return emptyElement('col')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function colgroup(Attribute ...$attributes): Closure
{
    return element('colgroup')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function table(Attribute ...$attributes): Closure
{
    return element('table')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function tbody(Attribute ...$attributes): Closure
{
    return element('tbody')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function td(Attribute ...$attributes): Closure
{
    return element('td')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function tfoot(Attribute ...$attributes): Closure
{
    return element('tfoot')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function th(Attribute ...$attributes): Closure
{
    return element('th')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function thead(Attribute ...$attributes): Closure
{
    return element('thead')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function tr(Attribute ...$attributes): Closure
{
    return element('tr')(...$attributes);
}


// Forms

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function button(Attribute ...$attributes): Closure
{
    return element('button')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function datalist(Attribute ...$attributes): Closure
{
    return element('datalist')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function fieldset(Attribute ...$attributes): Closure
{
    return element('fieldset')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function form(Attribute ...$attributes): Closure
{
    return element('form')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function input(Attribute ...$attributes): Closure
{
    return emptyElement('input')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function label(Attribute ...$attributes): Closure
{
    return element('label')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function legend(Attribute ...$attributes): Closure
{
    return element('legend')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function meter(Attribute ...$attributes): Closure
{
    return element('meter')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function optgroup(Attribute ...$attributes): Closure
{
    return element('optgroup')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function option(Attribute ...$attributes): Closure
{
    return element('option')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function output(Attribute ...$attributes): Closure
{
    return element('output')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function progress(Attribute ...$attributes): Closure
{
    return element('progress')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function select(Attribute ...$attributes): Closure
{
    return element('select')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function texarea(Attribute ...$attributes): Closure
{
    return element('texarea')(...$attributes);
}


// Interactive elements

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function details(Attribute ...$attributes): Closure
{
    return element('details')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function dialog(Attribute ...$attributes): Closure
{
    return element('dialog')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function menu(Attribute ...$attributes): Closure
{
    return element('menu')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function menuitem(Attribute ...$attributes): Closure
{
    return emptyElement('menuitem')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function summary(Attribute ...$attributes): Closure
{
    return element('summary')(...$attributes);
}


// Web components

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function shadow(Attribute ...$attributes): Closure
{
    return element('shadow')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function slot(Attribute ...$attributes): Closure
{
    return element('slot')(...$attributes);
}

/**
 * @param Attribute[] $attributes
 * @return Closure
 */
function template(Attribute ...$attributes): Closure
{
    return element('template')(...$attributes);
}


// Data model

interface Node
{
}

class RootElement implements Node
{
    /** @var Attribute[] */
    private $attributes;
    /** @var Node[] */
    private $children;

    /**
     * @param Attribute[] $attributes
     * @param Node[] $children
     */
    public function __construct($attributes, $children)
    {
        $this->attributes = $attributes;
        $this->children = $children;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return Node[]
     */
    public function getChildren()
    {
        return $this->children;
    }
}

class Element implements Node
{
    /** @var string */
    private $name;
    /** @var Attribute[] */
    private $attributes;
    /** @var Node[] */
    private $children;

    /**
     * @param string $name
     * @param Attribute[] $attributes
     * @param Node[] $children
     */
    public function __construct(string $name, $attributes, $children)
    {
        $this->name = $name;
        $this->attributes = $attributes;
        $this->children = $children;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }

    /**
     * @return Node[]
     */
    public function getChildren()
    {
        return $this->children;
    }
}

class EmptyElement implements Node
{
    /** @var string */
    private $name;
    /** @var Attribute[] */
    private $attributes;

    /**
     * @param string $name
     * @param Attribute[] $attributes
     */
    public function __construct(string $name, $attributes)
    {
        $this->name = $name;
        $this->attributes = $attributes;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return Attribute[]
     */
    public function getAttributes()
    {
        return $this->attributes;
    }
}

class Text implements Node
{
    /** @var string */
    private $value;

    /**
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}

class RawHtml implements Node
{
    /** @var string */
    private $html;

    /**
     * @param string $html
     */
    public function __construct(string $html)
    {
        $this->html = $html;
    }

    /**
     * @return string
     */
    public function getHtml(): string
    {
        return $this->html;
    }
}

class None implements Node
{
}
